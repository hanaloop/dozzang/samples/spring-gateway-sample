# docker build --build-arg JAR_FILE=build/libs/SpringGateway-0.0.1-SNAPSHOT.jar -t test/spring-gateway .
FROM openjdk:11-jre-slim-buster
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
