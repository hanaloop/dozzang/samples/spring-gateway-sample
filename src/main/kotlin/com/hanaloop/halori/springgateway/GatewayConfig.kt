package com.hanaloop.halori.springgateway

import org.springframework.cloud.gateway.filter.factory.TokenRelayGatewayFilterFactory
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.GatewayFilterSpec
import org.springframework.cloud.gateway.route.builder.PredicateSpec
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class GatewayConfig (val filterFactory: TokenRelayGatewayFilterFactory) {

    @Bean
    open fun customRouteLocator(builder: RouteLocatorBuilder): RouteLocator? {
        return builder.routes()
            .route(
                "resource"
            ) { r: PredicateSpec ->
                r.path("/resource/**")
                    .filters { f: GatewayFilterSpec ->
                        f.filters(filterFactory.apply())
                            .removeRequestHeader("Cookie")
                            .rewritePath("/resource/(?<segment>.*)", "/api/\${segment}")
                    } // Prevents cookie being sent downstream

                    .uri("http://localhost:9080")
            } // Taking advantage of docker naming
            .build()
    }
}