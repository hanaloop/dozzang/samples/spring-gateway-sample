package com.hanaloop.halori.springgateway

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.gateway.filter.factory.TokenRelayGatewayFilterFactory

@SpringBootApplication
class SpringGatewayApplication

fun main(args: Array<String>) {
	runApplication<SpringGatewayApplication>(*args)

}
